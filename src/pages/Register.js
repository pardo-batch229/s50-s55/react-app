import {Form, Button} from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Register(){

    const {user} = useContext(UserContext)

    const navigate = useNavigate()

    // State Hooks -> store values of the input fields
    const [email, setEmail] = useState('')
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [mobileNo, setMobileNo] = useState('')

    const [password1, setPassword1] = useState('')
    const [password2, setPassword2] = useState('')

    const [isActive, setIsActive] = useState(false)

    // console.log(email)
    // console.log(password1)
    // console.log(password2)

    useEffect(()=>{
        const mobileNumValid = mobileNo.length > 10;
        // Validation to enable register button when all fields are populated and both password match
        if((email !== '' && password1 !== '' && password2 !== '' && firstName !== '' && lastName !== '' && mobileNo !== '') && (password1 === password2) && mobileNumValid){
            setIsActive(true)
        }else{
            setIsActive(false)
        }
    },[email, password1, password2, firstName, lastName, mobileNo])

    
    // Function to simulate user registration

    function registerUser(e){
        // Prevents from reloading pages
        e.preventDefault()

        fetch(`${ process.env.REACT_APP_API_URL }/users/checkEmail`,{
            method: 'POST',
            body: JSON.stringify({email: email}),
            headers: { "Content-Type": "application/json" }
        })
        .then(res => res.json())
        .then(data =>{
            
            if(data === true){
                Swal.fire({
                    title: "Email already exists",
                    icon: "error",
                    text: "Please type another email"
                  })
            }else{
                fetch(`${ process.env.REACT_APP_API_URL }/users/register`,{
                    method: 'POST',
                    body: JSON.stringify({
                        email: email,
                        password: password1,
                        firstName: firstName,
                        lastName: lastName,
                        mobileNo: mobileNo
                    }),
                    headers: {"Content-Type": "application/json"}
                })
                .then(res => res.json())
                .then(data => {

                    if(data === true){
                        Swal.fire({
                            title: "Successfully Registered",
                            icon: "success",
                            text: "You have successfully registered!"              
                          })
                          setEmail("")
                          setFirstName("")
                          setLastName("")
                          setMobileNo("")
                          setPassword1("")
                          setPassword2("")
                          navigate("/login")          
                    }else{
                        Swal.fire({
                            title: "Oppps! Something went wrong",
                            icon: "error",
                            text: "Please try again"
                        })
                    }
                })
            }
        })
    }


    return(
        (user.id !== null) ?
            <Navigate to="/courses"/>
        :

        <Form onSubmit={(e)=> registerUser(e)}>
            <Form.Group className="mb-3" controlid="userFirstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control type="text" placeholder="Enter first name" value={firstName} onChange={event => setFirstName(event.target.value)} required/>
            </Form.Group>

            <Form.Group className="mb-3" controlid="userLastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control type="text" placeholder="Enter last name" value={lastName} onChange={event => setLastName(event.target.value)} required/>
            </Form.Group>

            <Form.Group className="mb-3" controlid="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control type="email" placeholder="Enter email" value={email} onChange={event => setEmail(event.target.value)} required/>
                <Form.Text className="text-muted">
                We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group className="mb-3" controlid="userMobileNo">
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control type="text" placeholder="Enter mobile number" value={mobileNo} onChange={event => setMobileNo(event.target.value)} required/>
            </Form.Group>

            <Form.Group className="mb-3" controlid="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control type="password" placeholder="Password" value={password1} onChange={event => setPassword1(event.target.value)}/>
            </Form.Group>

            <Form.Group className="mb-3" controlid="password2">
                <Form.Label>Confirm Password</Form.Label>
                <Form.Control type="password" placeholder="Confirm Password" value={password2} onChange={event => setPassword2(event.target.value)}/>
            </Form.Group>
             {/* Conditional Rendering */}
             {/* if active buton is clickable -> if Inactive button is not clickable */}
            {
                (isActive) ?
                <Button variant="primary" type="submit" controlid="submitBtn">
                    Register
                </Button>
                :
                <Button variant="primary" type="submit" controlid="submitBtn" disabled>
                    Register
                </Button>
            }
            
    </Form>
    )
}