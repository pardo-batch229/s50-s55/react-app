import Banner from '../components/Banner'
import Highlights from '../components/Highlights'

const data ={
    title: "Zuitt Coding Bootcamp",
    content: "Opportunities for everyone, everywhere!",
    destination: "/",
    label: "Enroll Now!"
}

export default function Home(){
    return(
        <>
            <Banner data={data}/>
            <Highlights/>
            
        </>
    )
}