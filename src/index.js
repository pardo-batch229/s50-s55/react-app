import React from 'react';
import ReactDOM from 'react-dom/client';
//import the bootstrap CSS
import 'bootstrap/dist/css/bootstrap.min.css'
// AppNavbar Importation
import App from './App';



const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    {/* <App /> */}
    <App/>
  </React.StrictMode>
);

// SPA- Single Page Application/ReactJs
// is a web application that runs on a single web page, dynamically updating the content without requiring the page to reload. SPA applications provide a smoother user experience by minimizing the need for full page reloads, resulting in faster and more responsive interactions.
// Some common examples of SPA applications include Gmail, Facebook, and Trello. These applications provide a fast, responsive, and intuitive user experience, which is why SPAs are becoming increasingly popular for building modern web applications.

/* const name = "John Smith";
const user = {
  firstName: 'Jane',
  lastName: 'Smith'
}

function formatName(user){
  return user.firstName + ' ' + user.lastName
}

const element = <h1>Hello, {formatName(user)}</h1>

root.render(element); */