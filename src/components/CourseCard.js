import {Card, Button} from 'react-bootstrap'
import { useState } from 'react'
import { Link } from 'react-router-dom'

export default function CourseCard({courseProps}){
  // Checks to see if the data was successfully passed
  console.log(courseProps)
  console.log(typeof courseProps)

  // Destructuring the data to avoid dot notation
  const {name, description, price, _id} = courseProps
  // 3 Hooks in React

  // 1. useState
  // 2. useEffect
  // 3. useContext

  //Use the useState hook for the component to be able to store state
  // States are used to keep track of information related to individual components
  // Syntax -> const [getter, setter] = useState(initialGetterValue)

  const [count, setCount] = useState(0)
  console.log(useState(0))
  const [availableSeat, setSeat] = useState(30)
 
  // function enroll(){
  //   console.log("Enrollees" + count)
   
  //   if(availableSeat > 0){
  //     //deduct when user enroll
  //     setSeat(availableSeat - 1)
  //     //increment to count when user enroll
  //     setCount(count + 1)
  //   }else{
  //     alert("No more seats")
  //   }
  // }

    return(
      <Card className='mx-4 my-3'>
        <Card.Body>
            <Card.Title>{name}</Card.Title>
            <Card.Subtitle>Description:</Card.Subtitle>
            <Card.Text>
            {description}
            </Card.Text>
            <Card.Subtitle>Price:</Card.Subtitle>
            <Card.Text>
            {price}
            </Card.Text>
            <Card.Subtitle className='mb-3'>Enrollees: </Card.Subtitle>
            <Card.Text>{count} Enrollees</Card.Text>
             <Card.Text>Slot Available: {availableSeat} </Card.Text> 
            {/* <Button onClick={enroll} variant="primary">Enroll</Button> */}
            <Link className='btn btn-primary' to={`/courseView/${_id}`}>Details</Link>
        </Card.Body>
      </Card>
        
    )
}