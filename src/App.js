import './App.css'
import { Container } from 'react-bootstrap'
import { useState, useEffect } from 'react'
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom'
import AppNavbar from './components/AppNavbar.js'
import CourseView from './components/CourseView'
import Home from './pages/Home'
import Courses from './pages/Courses.js'
import Register from './pages/Register.js'
import Login from './pages/Login.js'
import PageNotFound from './pages/pageNotFound.js'
import { UserProvider } from './UserContext.js'
import Logout from './pages/Logout.js'

// React JS is a single page application (SPA)
			// Whenever a link is clicked, it functions as if the page is being reloaded but what it actually does is it goes through the process of rendering, mounting, rerendering and unmounting components
			// When a link is clicked, React JS changes the url of the application to mirror how HTML accesses its urls
			// It renders the component executing the function component and it's expressions
			// After rendering it mounts the component displaying the elements
			// Whenever a state is updated or changes are made with React JS, it rerenders the component
			// Lastly, when a different page is loaded, it unmounts the component and repeats this process
			// The updating of the user interface closely mirrors that of how HTML deals with page navigation with the exception that React JS does not reload the whole page


function App(){

  const [user, setUser] = useState({
    /* 
      Syntax: 
        localStorage.getItem(propertyName)
        email: localStorage.getItem('email')
    
    */
    id: null,
    isAdmin: null,
  })

  const unsetUser = () => {
    localStorage.clear()
  }

  useEffect(()=>{

    console.log(user)
    console.log(localStorage)

  })

  return (
    // Fragment "<> and </>"
     /* WE are mounting our components and to prepare for output rendering */
     <UserProvider value={{user, setUser, unsetUser}}>
        <>
        <Router>
            <AppNavbar/>
            <Container>
              <Routes>
                {/* <Home/>
                    <Courses/> 
                    <Register/> 
                    <Login/> */}
                    <Route exact path="/" element={<Home/>}></Route>
                    <Route exact path="/courses/" element={<Courses/>}></Route>
                    <Route exact path="/login" element={<Login/>}></Route>
                    <Route exact path="/register" element={<Register/>}></Route>
                    <Route exact path="/logout" element={<Logout/>}></Route>
                    <Route exact path="/courseView/:courseId" element={<CourseView/>}></Route>
                    <Route exact path="/*" element={<PageNotFound/>}></Route>
              </Routes>
            </Container>
        </Router>
        </>
     </UserProvider>
  )
}

export default App
